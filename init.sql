CREATE DATABASE  IF NOT EXISTS `trcommunity` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `trcommunity`;
-- MySQL dump 10.13  Distrib 5.6.19, for Win32 (x86)
--
-- Host: 192.168.1.2    Database: trcommunity
-- ------------------------------------------------------
-- Server version	5.6.14-enterprise-commercial-advanced

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tc_columnmetas`
--

DROP TABLE IF EXISTS `tc_columnmetas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tc_columnmetas` (
  `cm_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `alias` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `sort` int(11) DEFAULT NULL,
  `tablemeta_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`cm_id`),
  KEY `tc_table_column_fk_idx` (`tablemeta_id`),
  CONSTRAINT `tc_table_column_fk` FOREIGN KEY (`tablemeta_id`) REFERENCES `tc_tablemetas` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tc_columnmetas`
--

LOCK TABLES `tc_columnmetas` WRITE;
/*!40000 ALTER TABLE `tc_columnmetas` DISABLE KEYS */;
/*!40000 ALTER TABLE `tc_columnmetas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tc_tablemetas`
--

DROP TABLE IF EXISTS `tc_tablemetas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tc_tablemetas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `menuid` varchar(255) DEFAULT NULL,
  `communityid` varchar(255) DEFAULT NULL,
  `tablename` varchar(255) DEFAULT NULL,
  `mongocollectionname` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tc_tablemetas`
--

LOCK TABLES `tc_tablemetas` WRITE;
/*!40000 ALTER TABLE `tc_tablemetas` DISABLE KEYS */;
/*!40000 ALTER TABLE `tc_tablemetas` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-05-29 16:37:43
