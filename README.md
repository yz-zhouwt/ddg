#DDG

基于NoSQL的动态数据业务治理系统——DDG(Dynamic Data Govern)system

使用到的技术包括不限于以下内容（排名不分先后，想到那个写那个）：

v1.0

1. Gradle
2. Groovy
3. SpringMVC
4. Freemarker
5. MongoDB
6. MyBatis
7. Logback

说明：
===================================================
该系统主要是为了应对复杂多变的数据库变化需求，将传统的DAO数据层从业务系统解耦，可以无缝集成到所有的业务系统中，完成所有DAO的接口实现。