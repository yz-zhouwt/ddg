package test.com.trgis.qtmap.commons.database.mongo;

import java.net.UnknownHostException;
import java.util.Date;

import com.mongodb.BasicDBObject;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;

/**
 * 
 * @author zhangqian
 * 
 *         MongoDB 连接实例
 *
 */
public class MongDBConnectionDemo {

	static String dbName = "trcommunity";
	static String collectionName = "76dd9e7fc97f4c058035b96f558bbd9b";

	public static void main(String[] args) throws UnknownHostException {
		pageQuery();
	}

	// 插入数据
	public static void insertData() throws UnknownHostException {
		TRMongoDB trdb = new TRMongoDB(dbName);

		DBObject obj = null;
		for (int i = 0; i < 10000000; i++) {
			obj = new BasicDBObject();
			obj.put("name", "zhangqian" + i);
			trdb.insertDBObject(collectionName, obj);
		}
	}

	// 分页查询
	public static void pageQuery() throws UnknownHostException {
		TRMongoDB trdb = new TRMongoDB(dbName);
		DBCursor cursor = trdb.getDBCursor(collectionName);
		while (cursor.hasNext()) {
			DBObject obj = cursor.next();
			System.out.println(obj);
			String name = obj.get("name").toString();
			System.out.println(name);
		}
	}
}
