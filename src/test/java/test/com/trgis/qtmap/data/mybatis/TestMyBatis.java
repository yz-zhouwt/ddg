package test.com.trgis.qtmap.data.mybatis;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.trgis.qtmap.community.modle.ColumnMeta;
import com.trgis.qtmap.community.modle.TableMeta;
import com.trgis.qtmap.community.service.SchemaManagementService;
import com.trgis.qtmap.data.mongodb.TRMongoRepository;
import com.trgis.qtmap.utils.UUIDUtils;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"classpath:applicationContext-*.xml"})
public class TestMyBatis {

	@Autowired
	private SchemaManagementService schemaManagementService;
	
	@Autowired
	private TRMongoRepository trMongoRepository;
	
	@Test
	public void testCreateTable(){
		// TODO
		TableMeta tableMeta = new TableMeta();
		ColumnMeta e = new ColumnMeta();
		e.setName("name");
		e.setAlias("姓名");
		e.setValueType("String");
		tableMeta.setMongoCollectionName(UUIDUtils.getRandomUUID());
		tableMeta.setMenuId("人员管理");
		tableMeta.setCommunityId("XXX社区");
		tableMeta.setTableName("人员管理");
		schemaManagementService.insertTable(tableMeta);
		System.out.println("新增的表的ID是："+tableMeta.getId());
		System.out.println("新增列");
		e.setTableMeta(tableMeta.getId());
		schemaManagementService.insertColumn(e);
	}
	
	// TODO 测试Groovy代码
	public void testGroovyString(){
		
	}
	
	
	@Test
	public void testInsertData(){
		Long tableId = 3l;
		TableMeta tm = schemaManagementService.findOneTable(tableId);
		String mc = tm.getMongoCollectionName();
		System.out.println(mc);
		Map<String,Object> dbobject = new HashMap<String,Object>();
		DBObject obj = new BasicDBObject();
		dbobject.put("name", "张三");
		dbobject.put("gender", "男");
		dbobject.put("age", 28);
		obj.putAll(dbobject);
		trMongoRepository.insert(mc, obj);
		
	}
	
	@Test
	public void testFindAll(){
		List<TableMeta> result = schemaManagementService.findAllTable();
		for (TableMeta tableMeta : result) {
			for (ColumnMeta colMeta : tableMeta.getColumns()) {
				System.out.println(colMeta.getName());
			}
		}
	}
	
	@Test
	public void testFindOne(){
		TableMeta result = schemaManagementService.findOneTable(12l);
		System.out.println(result.getMongoCollectionName());
		System.out.println(result.getColumns().size());
	}
}
