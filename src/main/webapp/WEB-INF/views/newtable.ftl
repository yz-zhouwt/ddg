<!Doctype html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="http://cdn.bootcss.com/bootstrap/3.3.4/css/bootstrap.min.css" rel="stylesheet">
<title></title>
</head>
<body>
	
    <#include "menu.ftl">
    
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-6 col-md-offset-3">
				<div class="form-group form-inline">
					<label for="tableName">表名:</label> <input type="text"
						class="form-control" id="tableName" placeholder="请输入表名">
				</div>
			</div>
			<div class="span8">
				<button id="complateTable" type="button"
					class="btn btn-lg btn-primary">
					<span class="glyphicon glyphicon-success" aria-hidden="true"></span>
					完成
				</button>
			</div>
		</div>
		<div class="row">
			<div class="col-md-8 col-md-offset-3">
				<div class="form-inline" role="form">
					<div class="form-group">
						<label for="colName">列名:</label> <input type="text"
							class="form-control" id="colName" placeholder="请输入列名">
					</div>
					<div class="form-group">
						<label for="colCode">代码:</label> <input type="text"
							class="form-control" id="colCode" placeholder="请输入代码">
					</div>
					<div class="form-group">
						<label for="type">类型:</label> <select id="type"
							class="form-control">
							<option value="String">字符</option>
							<option value="Integer">数字</option>
							<option value="DateTime">时间</option>
						</select>
					</div>
					<button id="addColMeta" type="button" class="btn btn-success">
						<span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
						添加
					</button>
				</div>
			</div>
		</div>

		<div class="row-fluid">
			<div class="span6">
				<div class="form-group">
					<ul id="tableMetaDatas" class="list-group">
					</ul>
				</div>
			</div>
		</div>
	</div>
	</div>

	<script src="http://cdn.bootcss.com/jquery/2.1.4/jquery.min.js"></script>
	<script src="http://cdn.bootcss.com/bootstrap/3.3.4/js/bootstrap.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			//点击添加按钮
			$('#addColMeta').on('click',function() {
				var colName = $('#colName').val();
				var colCode = $('#colCode').val();
				var colType = $('#type').val();
				var value = "{\"name\":\""+colCode+"\",\"alias\":\""+colName+"\",\"valueType\":\""+colType+"\"}"
				$('#tableMetaDatas').append(
						"<li class=\"list-group-item\" value=" +value+">列名：" 
						+ colName + "  代码：" 
						+ colCode + "  类型：" 
						+ colType + "</li>");
				});
			
			//点击完成
			$('#complateTable').on('click',function(){
				var columns = [];
				$('#tableMetaDatas > li').each(function(i){
					columns[i]=$(this).attr('value');
				})
				
				var tableName = $('#tableName').val();
				$.post("${host}table/save", {'tableName':tableName , 'columns': columns },function(){
					window.location.href="${host}table/list";
				});
			});
		});
	</script>
</body>
</html>