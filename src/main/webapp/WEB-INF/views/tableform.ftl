<!Doctype html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="http://cdn.bootcss.com/bootstrap/3.3.4/css/bootstrap.min.css" rel="stylesheet">
<title>添加数据</title>
</head>
<body>
    <#include "menu.ftl">
    
    <div class="container">
        <div class="page-header">
            <h2> ${table.tableName} <small>数据添加表单</small></h2>
        </div>
       
        <form class="form-horizontal" action="${host}table/savedata" method="post">
            <input type="hidden" name="id" value="${table.id}">
        <#list table.columns as col>
            <div class="input-group input-group-lg">
                <span class="input-group-addon" id="${col.name}">${col.alias}</span>
                <input type="text" class="form-control" name="${col.name}" placeholder="请输入${col.alias}" aria-describedby="${col.name}"/>
            </div>
            <br/>
        </#list>
            <div class="controls">
                <input type="submit" class="btn btn-success"></button>
            </div>
        </form>
    </div>
    
	<script src="http://cdn.bootcss.com/jquery/2.1.4/jquery.min.js"></script>
	<script src="http://cdn.bootcss.com/bootstrap/3.3.4/js/bootstrap.js"></script>
</body>
</html>