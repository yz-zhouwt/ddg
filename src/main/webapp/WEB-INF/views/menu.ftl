    <!-- 菜单 -->
    <div role="navigation" class="navbar navbar-default">
        <div class="navbar-header">
            <a class="navbar-brand">社区自由构建表单案例</a>
        </div>
        <div>
            <ul class="nav navbar-nav">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">表管理<b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li><a href="${host}table/list">查询列表</a></li>
                        <li><a href="${host}newtable">新增表格</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>