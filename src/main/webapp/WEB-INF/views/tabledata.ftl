<!Doctype html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="http://cdn.bootcss.com/bootstrap/3.3.4/css/bootstrap.min.css" rel="stylesheet">
<title>数据列表</title>
</head>
<body>
    <#include "menu.ftl">
    
    <div class="container">
        <div class="page-header">
            <h2> ${table.tableName} <small>数据列表</small></h2>
        </div>
       
        <@mongoresult list=results columns=table.columns>
        </@mongoresult>
    </div>
    
	<script src="http://cdn.bootcss.com/jquery/2.1.4/jquery.min.js"></script>
	<script src="http://cdn.bootcss.com/bootstrap/3.3.4/js/bootstrap.js"></script>
</body>
</html>