<!Doctype html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="http://cdn.bootcss.com/bootstrap/3.3.4/css/bootstrap.min.css" rel="stylesheet">
<script src="http://cdn.bootcss.com/jquery/2.1.4/jquery.min.js"></script>
<script src="http://cdn.bootcss.com/bootstrap/3.3.4/js/bootstrap.js"></script>
<title>表列表</title>
</head>
<body>

	<#include "menu.ftl">
	
    <div class="container-fluid">
    <div class="row">
    <div class="col-md-8 col-md-offset-2">
    <table class="table table-bordered table-hover">
    	<thead>    
	   		<tr class="active">
                <th width="50px;">编号</th>
                <th>名称</th>
                <th>表格式</th>
                <th width="120px;">操作</th>
            </tr>
		</thead>
        <tbody>
            <#list tables as table>
            <tr>
                <td class="active">${table.id}</td>
                <td>${table.tableName}</td>
                <td><#list table.columns as col>${col.name}</#list></td>
                <td><a href="${host}table/showdata/${table.id}/1">查询</a>|<a href="${host}table/getform/${table.id}">添加数据</a>|<a href="${host}deltable/${table.id}">删除</a></td>
            </tr>
            </#list>
        </tbody>
    </table>
	</div>
	</div>
	</div>
</body>
</html>