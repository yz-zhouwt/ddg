<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>MongoDB</title>
<link rel="stylesheet" href="http://cdn.bootcss.com/bootstrap/3.3.4/css/bootstrap.min.css">
</head>
<body>
	<div role="navigation" class="navbar navbar-default ">
		<div class="container-fluid">
			<div class="navbar-header">
				<a class="navbar-brand menu-btn" href="#" link="main.jsp">社区综合管理系统</a>
			</div>
			<ul class="nav navbar-nav">
				<li class="navitem"><a id="nav-menu" class="menu-btn" href="#" link="http://localhost:8080/trmongo/table/list">表管理</a></li>
			</ul>
		</div>
	</div>
	<div class="embed-responsive embed-responsive-16by9" style="padding-bottom: 41.25%;">
		<iframe id="if-container" class="embed-responsive-item" src="main.jsp" height="400px;"></iframe>
	</div>

	<script src="http://cdn.bootcss.com/jquery/1.11.2/jquery.min.js"></script>
	<script src="http://cdn.bootcss.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			$('.menu-btn').on("click", function(evt) {
				$("li.active").removeClass("active")
				$(this).parent().addClass("active")
				$('#if-container').attr("src", $(this).attr('link'));
			});
		});
	</script>
</body>
</html>