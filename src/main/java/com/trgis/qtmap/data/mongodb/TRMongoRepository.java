package com.trgis.qtmap.data.mongodb;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.CriteriaDefinition;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;
import com.mongodb.WriteResult;

@Repository
public class TRMongoRepository implements AbstractRepository {

	@Autowired
	private MongoTemplate mongoTemplate;

	public TRMongoRepository() {

	}

	@Override
	public void insert(String collectionName, DBObject object) {
		mongoTemplate.getCollection(collectionName).insert(object);
	}

	@Override
	public void removeOne(String collectionName, DBObject id) {
		mongoTemplate.getCollection(collectionName).remove(id);
	}

	@Override
	public WriteResult update(String collectionName, String colName,
			String originalName, Object modification) {
		Query query = new Query();
		query.addCriteria(Criteria.where("name").is(originalName));
		Update update = new Update();
		update.set("name", modification);
		return mongoTemplate.updateFirst(query, update, collectionName);
	}

	@Override
	public DBObject findOne(String collectionName, DBObject id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<DBObject> findAll(String collectionName) {
		Query query = new Query();
		query.limit(100);
		query.skip(0);
		return mongoTemplate.find(query, DBObject.class, collectionName);
	}

	@Override
	public List<DBObject> findByRegex(String collectionName, DBObject regex) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<DBObject> findPage(String collectionName,
			Map<String, Object> conditions, int pageIndex, int pageSize) {
		List<DBObject> resultList = null;
		// 如果没有条件
		if (null == conditions || conditions.isEmpty()) {
			Query query = new Query();
			query.skip((pageIndex-1)*pageSize).limit(pageSize);
			resultList = mongoTemplate.find(query,DBObject.class,collectionName);
		} else { // 有条件的查询
			Set<String> keys = conditions.keySet();
			CriteriaDefinition criteriaDefinition = null;
			for (String key : keys) {
				Object value = conditions.get(key);
				if (value == null) {// 如果该条件为空则不加入匹配规则中
					continue;
				}
				criteriaDefinition = Criteria.where(key).regex(value.toString());
			}
			Query query = new Query(criteriaDefinition);
			query.skip((pageIndex-1)*pageSize).limit(pageSize);
			resultList = mongoTemplate.find(query, DBObject.class);
		}
		return resultList;
	}

	@Override
	public DBCollection createCollection(String collectionName) {
		return mongoTemplate.createCollection(collectionName);
	}

	/**
	 * 删除Collection
	 * 
	 * @param collectionName
	 */
	public void dropCollection(String collectionName) {
		mongoTemplate.dropCollection(collectionName);
	}

}
