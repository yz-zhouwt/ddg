package com.trgis.qtmap.data.mongodb;

import java.util.List;
import java.util.Map;

import com.mongodb.DBCollection;
import com.mongodb.DBObject;
import com.mongodb.WriteResult;

/**
 * 抽象底层数据操作接口
 * 
 * @author 张谦
 *
 */
public interface AbstractRepository {

	/**
	 * 创建Collection
	 * @param collectionName
	 * @return 
	 */
	public DBCollection createCollection(String collectionName);
	
	/**
	 * 给指定表新增数据
	 * 
	 * @param object
	 */
	public void insert(String collectionName, DBObject object);

	/**
	 * 删除指定表数据
	 * 
	 * @param id
	 */
	public void removeOne(String collectionName, DBObject id);

	/**
	 * 更新指定表的数据
	 * 
	 * @param collectionName
	 * @param colName
	 * @param originalName 
	 * @param modification
	 */
	public WriteResult update(String collectionName,String colName,String originalName, Object modification);

	/**
	 * 查询指定表的一条记录
	 * 
	 * @param collectionName
	 * @param id
	 * @return
	 */
	public DBObject findOne(String collectionName, DBObject id);

	/**
	 * 查询指定表所有数据
	 * 
	 * @param collectionName
	 * @return
	 */
	public List<DBObject> findAll(String collectionName);

	/**
	 * 根据正则查询指定表的数据
	 * 
	 * @param collectionName
	 * @param regex
	 * @return
	 */
	public List<DBObject> findByRegex(String collectionName, DBObject regex);

	/**
	 * 根据正则查询指定表的分页数据
	 * 
	 * @param collectionName
	 * @param regex
	 * @return
	 */
	public List<DBObject> findPage(String collectionName, Map<String,Object> conditions,
			int pageIndex, int pageSize);
}
