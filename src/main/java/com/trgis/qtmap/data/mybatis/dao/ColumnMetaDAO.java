package com.trgis.qtmap.data.mybatis.dao;

import com.trgis.qtmap.community.modle.ColumnMeta;



/**
 * 列 DAO
 * @author zhangqian
 *
 */
public interface ColumnMetaDAO {
	
	static final String TABLE_NAME = "tc_columnmetas";
	static final String TABLE_PK = "id";
	
	
	/**
	 * 新增
	 * @param columnMeta
	 */
	void insert(ColumnMeta columnMeta);


	/**
	 * 删除对应表的列
	 * @param id
	 */
	void deleteTableColumns(Long id);
	

}
