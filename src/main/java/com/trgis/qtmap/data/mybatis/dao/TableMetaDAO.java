package com.trgis.qtmap.data.mybatis.dao;

import java.util.List;

import com.trgis.qtmap.community.modle.TableMeta;



/**
 * 业务表DAO
 * @author zhangqian
 *
 */
public interface TableMetaDAO {
	static final String TABLE_NAME = "tc_tablemetas";
	static final String TABLE_PK = "id";
	/**
	 * 添加业务表元数据
	 */
	public int insertTableMeta(TableMeta tableMeta);
	
	/**
	 * 查询所有数据表
	 * @return
	 */
	public List<TableMeta> findAll();

	/**
	 * 根据id查询表
	 * @param id
	 * @return
	 */
	public TableMeta findOne(Long id);

	/**
	 * 删除表格
	 * @param id
	 */
	public void deleteTableMeta(Long id);

}
