package com.trgis.qtmap.community.modle;

import java.util.List;

import org.springframework.data.annotation.TypeAlias;

import com.trgis.qtmap.utils.UUIDUtils;

/**
 * 系统业务表
 * 
 * @author zhangqian
 *
 */
@TypeAlias("TableMeta")
public class TableMeta {

	/**
	 * 主键 AI
	 */
	private Long id;

	/**
	 * 所属菜单 与权限系统对接
	 */
	private String menuId;

	/**
	 * 社区ID 定义查询范围
	 */
	private String communityId;

	/**
	 * 表名
	 */
	private String tableName;

	/**
	 * 对应mangodb的collectionName
	 */
	private String mongoCollectionName;

	/**
	 * 对应的列集合
	 */
	private List<ColumnMeta> columns;

	public TableMeta() {
	}

	public TableMeta(Long id, String menuId, String communityId,
			String tableName, String mongoCollectionName,
			List<ColumnMeta> columns) {
		this.id = id;
		this.menuId = menuId;
		this.communityId = communityId;
		this.tableName = tableName;
		this.mongoCollectionName = mongoCollectionName;
		this.columns = columns;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getMenuId() {
		return menuId;
	}

	public void setMenuId(String menuId) {
		this.menuId = menuId;
	}

	public String getCommunityId() {
		return communityId;
	}

	public void setCommunityId(String communityId) {
		this.communityId = communityId;
	}

	public String getTableName() {
		return tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	public List<ColumnMeta> getColumns() {
		return columns;
	}

	public void setColumns(List<ColumnMeta> columns) {
		this.columns = columns;
	}

	public String getMongoCollectionName() {
		if (mongoCollectionName == null) {
			mongoCollectionName = UUIDUtils.getRandomUUID();
		}
		return mongoCollectionName;
	}

	public void setMongoCollectionName(String mongoCollectionName) {
		this.mongoCollectionName = mongoCollectionName;
	}

}
