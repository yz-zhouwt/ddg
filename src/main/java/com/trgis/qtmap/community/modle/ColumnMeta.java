/**
 * 
 */
package com.trgis.qtmap.community.modle;

import org.springframework.data.annotation.TypeAlias;

/**
 * 列元数据表
 * 
 * @author zhangqian
 *
 */
@TypeAlias("columnMeta")
public class ColumnMeta {

	/**
	 * ID
	 */
	private Long id;

	/**
	 * 列名
	 */
	private String name;

	/**
	 * 别名
	 */
	private String alias;

	/**
	 * 类型
	 */
	private String valueType;

	/**
	 * 排序
	 */
	private Integer sort;

	/**
	 * column:tablemeta_id
	 * 
	 * 所属表格
	 */
	private Long tableMeta;

	public ColumnMeta() {
	}

	public ColumnMeta(Long id, String name, String alias, String valueType,
			Integer sort, Long tableMeta) {
		this.id = id;
		this.name = name;
		this.alias = alias;
		this.valueType = valueType;
		this.sort = sort;
		this.tableMeta = tableMeta;
	}

	public Long getTableMeta() {
		return tableMeta;
	}

	public void setTableMeta(Long tableMeta) {
		this.tableMeta = tableMeta;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAlias() {
		return alias;
	}

	public void setAlias(String alias) {
		this.alias = alias;
	}

	public String getValueType() {
		return valueType;
	}

	public void setValueType(String valueType) {
		this.valueType = valueType;
	}

	public Integer getSort() {
		return sort;
	}

	public void setSort(Integer sort) {
		this.sort = sort;
	}

}
