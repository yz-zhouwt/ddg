package com.trgis.qtmap.community.service;

import java.util.List;

import com.mongodb.DBObject;
import com.trgis.qtmap.community.modle.ColumnMeta;
import com.trgis.qtmap.community.modle.TableMeta;

/**
 * 表业务处理服务接口
 * 
 * @author zhangqian
 *
 */
public interface SchemaManagementService {

	void insertTable(TableMeta tableMeta);

	List<TableMeta> findAllTable();

	void deleteTableSchema(Long id);

	TableMeta findOneTable(Long id);

	void insertColumn(ColumnMeta e);

	/**
	 * 获取表单列
	 * @param id
	 * @return
	 */
	TableMeta getForm(Long id);

	void saveDataToMongo(String mongoCollectionName, DBObject data);

	
	
}
