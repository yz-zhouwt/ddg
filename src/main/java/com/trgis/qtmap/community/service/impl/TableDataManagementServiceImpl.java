/**
 * 
 */
package com.trgis.qtmap.community.service.impl;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mongodb.DBObject;
import com.trgis.qtmap.community.modle.TableMeta;
import com.trgis.qtmap.community.service.TableDataManagementService;
import com.trgis.qtmap.data.mongodb.TRMongoRepository;

/**
 * 表数据管理业务 实现类
 * 
 * @author zhangqian
 *
 */
@Service
public class TableDataManagementServiceImpl implements
		TableDataManagementService {

	private static final Logger log = LoggerFactory
			.getLogger(TableDataManagementServiceImpl.class);

	private static void debug(String msg) {
		log.debug(msg);
	}

	// ===============================================
	/**
	 * 分页单位
	 */
	private static final int PAGESIZE = 10;

	@Autowired
	private TRMongoRepository trMongoRepository;

	// ===============================================

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.trgis.qtmap.community.service.TableDataManagementService#findList
	 * (com.trgis.qtmap.community.modle.TableMeta,
	 * java.lang.Integer,java.lang.String)
	 */
	@Override
	public List<DBObject> findList(TableMeta tm, Integer pageIndex,
			Map<String,Object> conditions) {
		List<DBObject> list = null;
		if (tm != null) {
			debug("查询表" + tm.getTableName() + "的数据");
			debug("mangdb collection = " + tm.getMongoCollectionName());
			String collectionName = tm.getMongoCollectionName();
			list = trMongoRepository.findPage(collectionName, conditions, pageIndex,PAGESIZE);
		} else {
			debug("表参数错误!");
			throw new RuntimeException("表数据错误!");
		}

		return list;
	}

}
