/**
 * 
 */
package com.trgis.qtmap.community.service;

import java.util.List;
import java.util.Map;

import com.mongodb.DBObject;
import com.trgis.qtmap.community.modle.TableMeta;

/**
 * 表数据管理业务 接口类
 * 
 * @author zhangqian
 *
 */
public interface TableDataManagementService {

	/**
	 * 查询表数据
	 * 
	 * @param tm
	 * @param pageIndex 分页单位
	 * @param conditions 查询条件
	 * @return
	 */
	List<DBObject> findList(TableMeta tm, Integer pageIndex, Map<String,Object> conditions);

}
