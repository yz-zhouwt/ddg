package com.trgis.qtmap.web.controller;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.trgis.qtmap.community.modle.ColumnMeta;
import com.trgis.qtmap.community.modle.TableMeta;
import com.trgis.qtmap.community.service.SchemaManagementService;
import com.trgis.qtmap.community.service.TableDataManagementService;
import com.trgis.qtmap.utils.ParamTransforms;

@Controller
public class SchemaManagementController {

	/**
	 * 记录日志
	 */
	private static final Logger logger = LoggerFactory
			.getLogger(SchemaManagementController.class);

	private static final String ID = "id";

	/*
	 * debug
	 * 
	 * @param msg
	 */
	private static void debug(String msg) {
		logger.debug(msg);
	}

	/*
	 * ========================================================
	 */
	
	@Autowired
	private SchemaManagementService schemaManagementService;
	
	@Autowired
	private TableDataManagementService tableDataManagementService;

	/**
	 * 新增表
	 * 
	 * @param tableName
	 * @param columns
	 * @param mv
	 * @return
	 */
	@RequestMapping(value = "/table/save", method = RequestMethod.POST)
	public ModelAndView createTableSchema(
			@RequestParam("tableName") String tableName,
			@RequestParam("columns[]") List<String> columns, ModelAndView mv) {
		TableMeta tableMeta = new TableMeta();
		tableMeta.setTableName(tableName);
		tableMeta.setColumns(ParamTransforms.tranJSONTCol(columns));
		schemaManagementService.insertTable(tableMeta);
		mv.setViewName("redirect:/table/list");
		return mv;
	}

	/**
	 * 查询表列表
	 * 
	 * @param mv
	 * @return
	 */
	@RequestMapping(value = "/table/list", method = RequestMethod.GET)
	public ModelAndView showTableSchema(ModelAndView mv) {
		List<TableMeta> tables = schemaManagementService.findAllTable();
		mv.setViewName("tablelist");
		mv.addObject("tables", tables);
		return mv;
	}

	/**
	 * 删除表
	 * 
	 * @param id
	 * @param mv
	 * @return
	 */
	@RequestMapping(value = "/deltable/{id}", method = RequestMethod.GET)
	public ModelAndView delTableSchema(@PathVariable Long id, ModelAndView mv) {
		schemaManagementService.deleteTableSchema(id);
		mv.setViewName("redirect:/table/list");
		return mv;
	}

	/**
	 * 查询表
	 * 
	 * @param id
	 * @param mv
	 * @return
	 */
	@RequestMapping(value = "/table/getform/{id}", method = RequestMethod.GET)
	public ModelAndView createForm(@PathVariable Long id, ModelAndView mv) {
		logger.debug("返回ID=" + id);
		TableMeta table = schemaManagementService.findOneTable(id);
		if (table != null) {
			debug("查询到表格 " + table.getTableName());
			mv.addObject("table", table);
			mv.setViewName("tableform");
		}
		return mv;
	}

	/**
	 * 保存表数据
	 * 
	 * @param id
	 * @param request
	 * @param mv
	 * @return
	 */
	@RequestMapping(value = "/table/savedata", method = RequestMethod.POST)
	public ModelAndView insertData(@RequestParam Map<String, Object> values,
			ModelAndView mv) {
		/*
		 * 新增数据 1.查询需要新增数据的表格 2.根据表格属性从request中去抓取数据 3.组织数据结构 4.保存到MongDB中
		 */
		Long id = Long.parseLong(values.get(ID).toString());
		debug("需要新增数据的表格ID为:" + id);
		TableMeta tm = schemaManagementService.findOneTable(id);
		debug("查询到表:" + tm.getTableName());
		List<ColumnMeta> cms = tm.getColumns();
		debug("表列数:" + cms.size());

		DBObject data = new BasicDBObject();
		for (ColumnMeta columnMeta : cms) {
			String key = columnMeta.getName();
			debug("列:" + key);
			debug("取值=" + values.get(key));
			data.put(key, values.get(key));
		}
		schemaManagementService.saveDataToMongo(tm.getMongoCollectionName(),data);
		mv.setViewName("redirect:/table/list");
		return mv;
	}

	/**
	 * 显示表数据
	 * 
	 * @param id 表ID
	 * @param pageIndex 页码
	 * @param mv
	 * @return
	 */
	@RequestMapping(value = "/table/showdata/{id}/{pageIndex}")
	public ModelAndView showTableData(@PathVariable Long id,
			@PathVariable Integer pageIndex, ModelAndView mv) {
		TableMeta tm = schemaManagementService.findOneTable(id);
		mv.setViewName("tabledata");
		List<DBObject> results = tableDataManagementService.findList(tm,pageIndex,null);
		mv.addObject("table", tm);
		mv.addObject("results", results);
		return mv;
	}

}
