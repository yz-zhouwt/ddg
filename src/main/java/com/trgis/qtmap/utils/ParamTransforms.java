/**
 * 
 */
package com.trgis.qtmap.utils;

import java.util.ArrayList;
import java.util.List;

import com.trgis.qtmap.community.modle.ColumnMeta;
import com.trgis.utils.GVJSONUtils;

/**
 * 用于Controller接收参数的转换
 * 
 * @author zhangqian
 *
 */
public class ParamTransforms {

	public static String tranListTJson(String[] tableMeta) {
		StringBuffer json = new StringBuffer("[");
		for (String meta : tableMeta) {
			json.append(meta + ",");
		}
		json.deleteCharAt(json.lastIndexOf(","));
		json.append("]");
		return json.toString();
	}

	/**
	 * 转换字符串的json对象变为集合列表对象
	 * 
	 * @param tableMetaData
	 * @return
	 */
	public static List<String> transTMTL(String tableMetaData) {
		return null;
	}

	public static List<ColumnMeta> tranJSONTCol(List<String> columns) {
		List<ColumnMeta> cols = null;
		if (!isArrayEmpty(columns)) {
			cols = new ArrayList<ColumnMeta>();
			ColumnMeta cm = null;
			for (int i = 0; i < columns.size(); i++) {
				String jsonCol = columns.get(i);
				cm = (ColumnMeta) GVJSONUtils.convert(jsonCol, ColumnMeta.class);
				cols.add(cm);
			}
		}
		return cols;
	}

	/**
	 * 判断数组是否为空
	 * 
	 * @param array
	 * @return
	 */
	private static boolean isArrayEmpty(List<?> array) {
		if (null == array || array.isEmpty()) {
			return true;
		}
		return false;
	}

}
